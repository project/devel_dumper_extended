<?php

namespace Drupal\devel_dumper_extended\Plugin\Devel\Dumper;

use Drupal\devel\DevelDumperBase;

/**
 * Provides a Xdebug-based dumper plugin.
 *
 * @DevelDumper(
 *   id = "xdebug",
 *   label = @Translation("Xdebug"),
 *   description = @Translation("Xdebug-based variable dumper."),
 * )
 */
class XDebug extends DevelDumperBase {

  /**
   * {@inheritdoc}
   */
  public function export($input, $name = NULL) {
    if (function_exists('xdebug_break')) {
      xdebug_break();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function exportAsRenderable($input, $name = NULL) {
    if (function_exists('xdebug_break')) {
      xdebug_break();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function checkRequirements() {
    return extension_loaded('xdebug');
  }

}
