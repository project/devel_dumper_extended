<?php

namespace Drupal\devel_dumper_extended\Plugin\Devel\Dumper;

use Drupal\devel\DevelDumperBase;

/**
 * Provides a disabled dumper plugin.
 *
 * @DevelDumper(
 *   id = "disabled",
 *   label = @Translation("Disabled"),
 *   description = @Translation("Devel dumper disabled."),
 * )
 */
class Disabled extends DevelDumperBase {

  /**
   * {@inheritdoc}
   */
  public function export($input, $name = NULL) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function exportAsRenderable($input, $name = NULL) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function checkRequirements() {
    return TRUE;
  }

}
